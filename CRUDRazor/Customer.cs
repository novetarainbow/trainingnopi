﻿using System.ComponentModel.DataAnnotations;

namespace CRUDRazor
{
    public class Customer
    {
        public int Id { get; set; }
        [Required, StringLength(10)]
        public string Name { get; set; }
        public string Address { get; set; }
    }
}